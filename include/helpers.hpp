#pragma once

#include <cstddef>

namespace google::protobuf {
class FileDescriptor;
class Descriptor;
class FieldDescriptor;

namespace compiler::cpp {
class Formatter;
}  // namespace compiler::cpp

}  // namespace google::protobuf

namespace generation::helpers {
[[nodiscard]] std::size_t NestedCount(const google::protobuf::FileDescriptor *const file);
[[nodiscard]] std::size_t NestedCount(const google::protobuf::Descriptor *const type);

[[nodiscard]] std::size_t MapCount(const google::protobuf::FileDescriptor *const file);
[[nodiscard]] std::size_t MapCount(const google::protobuf::Descriptor *const type);

[[nodiscard]] bool HasString(const google::protobuf::FileDescriptor *const file);
[[nodiscard]] bool HasString(const google::protobuf::Descriptor *const type);

// Adds "container_type", "key_type", "value_type" variables to the map
void PushMapType(const google::protobuf::FieldDescriptor *const field,
                 google::protobuf::compiler::cpp::Formatter *const formatter);
// Adds "container_type", "type_name" variables to the map
void PushRepeatedType(const google::protobuf::FieldDescriptor *const field,
                      google::protobuf::compiler::cpp::Formatter *const formatter);
// Adds "container_type" variable to the map
void PushStringType(const google::protobuf::FieldDescriptor *const field,
                    google::protobuf::compiler::cpp::Formatter *const formatter);
}  // namespace generation::helpers
