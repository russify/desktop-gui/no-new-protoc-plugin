#pragma once
#include <google/protobuf/compiler/code_generator.h>

#include <string>

namespace google::protobuf {
class Descriptor;

namespace io {
class Printer;
}  // namespace io
}  // namespace google::protobuf

namespace generation {
class CodeGenerator : public google::protobuf::compiler::CodeGenerator {
 public:
  [[nodiscard]] bool Generate(const google::protobuf::FileDescriptor *file, const std::string &parameter,
                              google::protobuf::compiler::GeneratorContext *generator_context,
                              std::string *error) const override;

  [[nodiscard]] constexpr uint64_t GetSupportedFeatures() const override { return Feature::FEATURE_PROTO3_OPTIONAL; }
};
}  // namespace generation
