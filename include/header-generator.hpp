#pragma once

#include <string>

namespace google::protobuf {
class Descriptor;
class FileDescriptor;

namespace io {
class ZeroCopyOutputStream;
}
}  // namespace google::protobuf

namespace generation {
class HeaderGenerator {
 public:
  [[nodiscard]] bool GenerateHeader(const google::protobuf::FileDescriptor *const file,
                                    google::protobuf::io::ZeroCopyOutputStream *const output,
                                    std::string *const error) const;
};
}  // namespace generation
