#include "./generator.hpp"

#include <google/protobuf/compiler/cpp/helpers.h>
#include <google/protobuf/descriptor.h>

#include "./header-generator.hpp"
#include "./helpers.hpp"
#include "./source-generator.hpp"

using namespace google::protobuf;  // NOLINT build/namespaces

// TODO(shmuk): add Arena; allocate small messages on stack; allocate big object with allocator;

namespace {
[[nodiscard]] bool CheckConstraints(const FileDescriptor *const file, std::string *const error) {
  if (file->syntax() != FileDescriptor::SYNTAX_PROTO3) {
    error->assign("Only proto3 syntax is supported");
    return false;
  }

  if (file->public_dependency_count() != 0) {
    error->assign("Public dependencies are not supported");
    return false;
  }

  if (file->service_count()) {
    error->assign("Services are not supported");
    return false;
  }

  if (file->weak_dependency_count()) {
    error->assign("Weak dependencies are not supported");
    return false;
  }

  if (compiler::cpp::HasEnumDefinitions(file)) {
    error->assign("Enums are not supported");
    return false;
  }

  if (compiler::cpp::HasExtensionsOrExtendableMessage(file)) {
    error->assign("Extensions are not supported");
    return false;
  }

  // maps are messages and NestedCount takes maps into account
  if (generation::helpers::MapCount(file) != generation::helpers::NestedCount(file)) {
    error->assign("Nested types are not supported");
    return false;
  }

  return true;
}
}  // namespace

namespace generation {
bool CodeGenerator::Generate(const FileDescriptor *file, const std::string &parameter,
                             compiler::GeneratorContext *generator_context, std::string *error) const {
  if (!CheckConstraints(file, error)) {
    return false;
  }

  try {
    HeaderGenerator header_generator;
    std::unique_ptr<io::ZeroCopyOutputStream> output{
        generator_context->Open(compiler::StripProto(file->name()) + ".pb.h")};
    if (!header_generator.GenerateHeader(file, output.get(), error)) {
      return false;
    }

    SourceGenerator source_generator;
    if (!source_generator.GenerateSource(file, output.get(), error)) {
      return false;
    }

    return true;
  } catch (const std::exception &ex) {
    error->assign(ex.what());
  }
  return false;
}
}  // namespace generation
