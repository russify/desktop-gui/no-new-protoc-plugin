#include "./source-generator.hpp"

#include <google/protobuf/compiler/cpp/cpp_generator.h>
#include <google/protobuf/compiler/cpp/helpers.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/io/printer.h>
#include <google/protobuf/io/zero_copy_stream.h>

#include "./helpers.hpp"

using namespace google::protobuf;  // NOLINT build/namespaces

namespace generation {
namespace {
void GenerateSetter(compiler::cpp::Formatter *const format) {
  (*format)("constexpr void $nm$::Set$method_name$($field_type$ value) noexcept {\n");
  format->Indent();
  (*format)("$field_name$ = value;\noptional_fields[$op_index$] |= static_cast<std::byte>($op_subindex$);\n");
  format->Outdent();
  (*format)("}\n");
}

void GenerateGetter(compiler::cpp::Formatter *const format) {
  (*format)("constexpr $field_type$ $nm$::$method_name$() const noexcept {\n");
  format->Indent();
  (*format)("return $field_name$;\n");
  format->Outdent();
  (*format)("}\n");
}

void GenerateClear(compiler::cpp::Formatter *const format) {
  (*format)("constexpr void $nm$::Clear$method_name$() noexcept {\n");
  format->Indent();
  (*format)("optional_fields[$op_index$] &= ~static_cast<std::byte>($op_subindex$);\n");
  format->Outdent();
  (*format)("}\n");
}

void GenerateHas(compiler::cpp::Formatter *const format) {
  (*format)("constexpr bool $nm$::Has$method_name$() const noexcept {\n");
  format->Indent();
  (*format)(
      "return (optional_fields[$op_index$] & static_cast<std::byte>($op_subindex$)) != static_cast<std::byte>(0);\n");
  format->Outdent();
  (*format)("}\n");
}

void GenerateMessageHas(compiler::cpp::Formatter *const format) {
  (*format)("constexpr bool $nm$::Has$method_name$() const noexcept {\n");
  format->Indent();
  (*format)(
      "return (optional_fields[$op_index$] & static_cast<std::byte>($op_subindex$)) != static_cast<std::byte>(0);\n");
  format->Outdent();
  (*format)("}\n");
}

void GenerateMessageGetters(compiler::cpp::Formatter *const format) {
  (*format)("constexpr $field_type$ &$nm$::Get$method_name$() noexcept {\n");
  format->Indent();
  (*format)("return $field_name$;\n");
  format->Outdent();
  (*format)("}\n");

  (*format)("constexpr const $field_type$ &$nm$::Get$method_name$() const noexcept {\n");
  format->Indent();
  (*format)("return $field_name$;\n");
  format->Outdent();
  (*format)("}\n");
}

void GenerateStringSetter(compiler::cpp::Formatter *const format) {
  (*format)("constexpr void $nm$::Set$method_name$(const $container_type$ &value) {\n");
  format->Indent();
  (*format)(
      "$field_name$ = value;\n"
      "optional_fields[$op_index$] |= static_cast<std::byte>($op_subindex$);\n");
  format->Outdent();
  (*format)("}\n");

  (*format)("constexpr void $nm$::Set$method_name$($container_type$ &&value) noexcept {\n");
  format->Indent();
  (*format)(
      "$field_name$ = std::move(value);\n"
      "optional_fields[$op_index$] |= static_cast<std::byte>($op_subindex$);\n");
  format->Outdent();
  (*format)("}\n");
}

void GenerateStringGetter(compiler::cpp::Formatter *const format) {
  (*format)("constexpr const $container_type$ &$nm$::$method_name$() const noexcept {\n");
  format->Indent();
  (*format)("return $field_name$;\n");
  format->Outdent();
  (*format)("}\n");
}

void GenerateStringClear(compiler::cpp::Formatter *const format) {
  (*format)("constexpr void $nm$::Clear$method_name$() noexcept {\n");
  format->Indent();
  (*format)(
      "optional_fields[$op_index$] &= ~static_cast<std::byte>($op_subindex$);\n"
      "$field_name$.clear();\n");
  format->Outdent();
  (*format)("}\n");
}

void WriteOptional(const Descriptor *const type, const FieldDescriptor *const optional,
                   compiler::cpp::Formatter *const formatter, std::size_t index) {
  formatter->Set("op_index", std::to_string(index / 8));
  formatter->Set("op_subindex", std::to_string(1 << (index % 8)));
  switch (optional->type()) {
    case FieldDescriptor::Type::TYPE_MESSAGE:
      formatter->Set("field_type", optional->message_type()->name());
      GenerateMessageHas(formatter);
      GenerateMessageGetters(formatter);
      break;
    case FieldDescriptor::Type::TYPE_STRING:
    case FieldDescriptor::Type::TYPE_BYTES:
      helpers::PushStringType(optional, formatter);
      GenerateStringSetter(formatter);
      GenerateStringGetter(formatter);
      GenerateStringClear(formatter);
      GenerateHas(formatter);
      break;
    default:
      formatter->Set("field_type", compiler::cpp::PrimitiveTypeName(optional->cpp_type()));
      GenerateSetter(formatter);
      GenerateGetter(formatter);
      GenerateClear(formatter);
      GenerateHas(formatter);
  }
}

void WriteContainer(const FieldDescriptor *const field, compiler::cpp::Formatter *const format) {
  format->Set("field_name", compiler::cpp::FieldName(field));

  (*format)("constexpr $container_type$ &$nm$::$method_name$() noexcept {\n");
  format->Indent();
  (*format)("return $field_name$;\n");
  format->Outdent();
  (*format)("}\n");

  (*format)("constexpr const $container_type$ &$nm$::$method_name$() const noexcept {\n");
  format->Indent();
  (*format)("return $field_name$;\n");
  format->Outdent();
  (*format)("}\n");
}

void GenerateToJsonMethod(const Descriptor *const type, compiler::cpp::Formatter *const format) {
  (*format)("inline nlohmann::json $nm$::ToJson() const {\n");
  format->Indent();
  (*format)("nlohmann::json result;\n");
  const auto kFieldCount{type->field_count()};
  for (int field_index{}; field_index < kFieldCount; ++field_index) {
    const auto field{type->field(field_index)};
    format->Set("field_name", compiler::cpp::FieldName(field));
    if (field->type() == FieldDescriptor::Type::TYPE_MESSAGE) {
      (*format)("result[\"$field_name$\"] = $field_name$.ToJson();\n");
    } else {
      if (field->is_repeated() || field->is_map()) {
        (*format)("if (!$field_name$.empty()) {\n");
      } else {
        (*format)("if (Has$1$()) {\n", compiler::cpp::UnderscoresToCamelCase(compiler::cpp::FieldName(field), true));
      }

      format->Indent();
      (*format)("result[\"$field_name$\"] = $field_name$;\n");
      format->Outdent();
      (*format)("}\n\n");
    }
  }
  (*format)("return result;\n");
  format->Outdent();
  (*format)("}\n");
}

void GenerateFromJsonMethod(const Descriptor *const type, compiler::cpp::Formatter *const format) {
  (*format)("inline void $nm$::FromJson(const nlohmann::json &value) {\n");
  format->Indent();
  const auto kFieldCount{type->field_count()};
  (*format)("const auto cend{value.cend()};\n");
  for (int field_index{}; field_index < kFieldCount; ++field_index) {
    const auto field{type->field(field_index)};
    format->Set("field_name", compiler::cpp::FieldName(field));
    (*format)("if (value.find(\"$field_name$\") != cend) {\n");
    format->Indent();
    const auto type{field->type()};
    if (type == FieldDescriptor::Type::TYPE_MESSAGE) {
      (*format)("$field_name$.FromJson(value[\"$field_name$\"]);\n");
    } else if (type == FieldDescriptor::Type::TYPE_BYTES || type == FieldDescriptor::Type::TYPE_STRING) {
      helpers::PushStringType(field, format);
      (*format)("Set$1$(value[\"$field_name$\"].get<$container_type$>());\n",
                compiler::cpp::UnderscoresToCamelCase(compiler::cpp::FieldName(field), true));
    } else if (field->is_map()) {
      helpers::PushMapType(field, format);
      (*format)("$field_name$ = value[\"$field_name$\"].get<$container_type$>();\n");
    } else if (field->is_repeated()) {
      helpers::PushRepeatedType(field, format);
      (*format)("$field_name$ = value[\"$field_name$\"].get<$container_type$>();\n");
    } else {
      format->Set("field_type", compiler::cpp::PrimitiveTypeName(field->cpp_type()));
      format->Set("method_name", compiler::cpp::UnderscoresToCamelCase(compiler::cpp::FieldName(field), true));
      (*format)("Set$method_name$(value[\"$field_name$\"].get<$field_type$>());\n");
    }

    format->Outdent();
    (*format)("}\n");
  }
  format->Outdent();
  (*format)("}\n");
}

[[nodiscard]] bool AppendMessage(const Descriptor *const type, compiler::cpp::Formatter *const formatter,
                                 std::string *const error, std::size_t index) {
  const auto kFieldCount{type->field_count()};
  formatter->Set("nm", compiler::cpp::ClassName(type));
  for (std::size_t field_index{}; field_index < kFieldCount; ++field_index) {
    const auto field{type->field(field_index)};
    formatter->Set("field_name", compiler::cpp::FieldName(field));
    formatter->Set("method_name", compiler::cpp::UnderscoresToCamelCase(compiler::cpp::FieldName(field), true));

    (*formatter)("// $field_name$ methods vvvvvv\n\n");
    if (field->is_map()) {
      helpers::PushMapType(field, formatter);
      WriteContainer(field, formatter);
    } else if (field->is_repeated()) {
      helpers::PushRepeatedType(field, formatter);
      WriteContainer(field, formatter);
    } else {
      WriteOptional(type, field, formatter, index++);
    }

    (*formatter)("\n// $field_name$ methods ^^^^^^\n\n");
  }
  GenerateToJsonMethod(type, formatter);
  GenerateFromJsonMethod(type, formatter);
  return true;
}
}  // namespace

bool SourceGenerator::GenerateSource(const google::protobuf::FileDescriptor *const file,
                                     io::ZeroCopyOutputStream *const output, std::string *const error) const {
  io::Printer printer{output, '$'};

  compiler::cpp::Formatter format{&printer};
  format("\n\n// implementation starts here\n");
  compiler::cpp::NamespaceOpener nm{compiler::cpp::Namespace(file, {}), format};

  try {
    const auto kMessagesTypesCount{file->message_type_count()};
    for (int type_index{}; type_index < kMessagesTypesCount; ++type_index) {
      if (!AppendMessage(file->message_type(type_index), &format, error, 0)) {
        return false;
      }
    }

    return true;
  } catch (const std::exception &ex) {
    error->assign(ex.what());
  }
  return false;
}
}  // namespace generation
