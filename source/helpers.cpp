#include "./helpers.hpp"

#include <google/protobuf/compiler/cpp/helpers.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/io/printer.h>

#include <format>

using namespace google::protobuf;  // NOLINT build/namespaces

namespace generation::helpers {
std::size_t NestedCount(const FileDescriptor *const file) {
  std::size_t result{};
  const auto kMessageTypeCount{file->message_type_count()};
  for (int type_index{}; type_index < kMessageTypeCount; ++type_index) {
    result += NestedCount(file->message_type(type_index));
  }
  return result;
}

std::size_t NestedCount(const Descriptor *const type) {
  std::size_t result{static_cast<std::size_t>(type->nested_type_count())};
  const auto kFieldCount{type->field_count()};
  for (int field_index{}; field_index < kFieldCount; ++field_index) {
    const auto field{type->field(field_index)};
    if (field->type() == FieldDescriptor::Type::TYPE_MESSAGE) {
      result += NestedCount(field->message_type());
    }
  }
  return result;
}

std::size_t MapCount(const FileDescriptor *const file) {
  std::size_t result{};
  const auto kMessageTypeCount{file->message_type_count()};
  for (int type_index{}; type_index < kMessageTypeCount; ++type_index) {
    result += MapCount(file->message_type(type_index));
  }
  return result;
}

std::size_t MapCount(const Descriptor *const type) {
  std::size_t result{};
  const auto kFieldCount{type->field_count()};
  for (int field_index{}; field_index < kFieldCount; ++field_index) {
    const auto field{type->field(field_index)};

    // order does matter: map is Message
    if (field->is_map()) {
      ++result;
      continue;
    }

    if (field->type() == FieldDescriptor::Type::TYPE_MESSAGE) {
      result += MapCount(field->message_type());
    }
  }
  return result;
}

bool HasString(const google::protobuf::FileDescriptor *const file) {
  const auto kMessageCount{file->message_type_count()};
  for (int type_index{}; type_index < kMessageCount; ++type_index) {
    if (HasString(file->message_type(type_index))) {
      return true;
    }
  }
  return false;
}

bool HasString(const google::protobuf::Descriptor *const type) {
  const auto kFieldCount{type->field_count()};
  for (int field_index{}; field_index < kFieldCount; ++field_index) {
    const auto field{type->field(field_index)};
    switch (field->type()) {
      case FieldDescriptor::Type::TYPE_MESSAGE:
        if (HasString(field->message_type())) {
          return true;
        }
        break;

      case FieldDescriptor::Type::TYPE_STRING:
      case FieldDescriptor::Type::TYPE_BYTES:
        return true;
    }
  }
  return false;
}

void PushMapType(const FieldDescriptor *const field, compiler::cpp::Formatter *const formatter) {
  const auto map{field->message_type()};
  const char *const key_type{compiler::cpp::PrimitiveTypeName(map->map_key()->cpp_type())};
  const char *const value_type{compiler::cpp::PrimitiveTypeName(map->map_value()->cpp_type())};
  formatter->Set("key_type", key_type);
  formatter->Set("value_type", value_type);
  formatter->Set(
      "container_type",
      std::format("std::map<{0}, {1}, std::less<{0}>, ::utility::memory::Allocator<std::pair<const {0}, {1}>>>",
                  key_type, value_type));
}

void PushRepeatedType(const FieldDescriptor *const field, compiler::cpp::Formatter *const formatter) {
  const char *const type_name{compiler::cpp::PrimitiveTypeName(field->cpp_type())};
  formatter->Set("type_name", type_name);
  formatter->Set("container_type", std::format("std::vector<{0}, ::utility::memory::Allocator<{0}>>", type_name));
}

void PushStringType(const google::protobuf::FieldDescriptor *const field,
                    google::protobuf::compiler::cpp::Formatter *const formatter) {
  formatter->Set("container_type",
                 "std::basic_string<char, std::char_traits<char>, ::utility::memory::Allocator<char>>");
}
}  // namespace generation::helpers
