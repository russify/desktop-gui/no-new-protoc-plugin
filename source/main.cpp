#include <google/protobuf/compiler/plugin.h>

// local
#include <generator.hpp>
#include <thread>  // NOLINT

int main(int argc, char **argv) {
  // std::this_thread::sleep_for(std::chrono::seconds(10));
  generation::CodeGenerator generator;
  return google::protobuf::compiler::PluginMain(argc, argv, &generator);
}
