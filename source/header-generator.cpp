#include "./header-generator.hpp"

#include <google/protobuf/compiler/cpp/cpp_generator.h>
#include <google/protobuf/compiler/cpp/helpers.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/io/printer.h>
#include <google/protobuf/io/zero_copy_stream.h>

#include "./helpers.hpp"

using namespace google::protobuf;  // NOLINT build/namespaces

namespace {
[[nodiscard]] bool CheckConstraints(const Descriptor *const type, std::string *const error) {
  if (type->oneof_decl_count()) {
    error->assign("Oneofs are not supported");
    return false;
  }

  if (type->real_oneof_decl_count() != 0) {
    error->assign("Real oneofs (what is it?=)) are not supported");
    return false;
  }

  return true;
}

void WriteField(const FieldDescriptor *const field, io::Printer *const printer) {
  std::string field_type;
  const auto type{field->type()};
  if (type == FieldDescriptor::Type::TYPE_MESSAGE) {
    field_type = field->message_type()->name();
  } else {
    field_type = compiler::cpp::PrimitiveTypeName(field->cpp_type());
  }
  compiler::cpp::Formatter formatter{printer};
  formatter.Set("field_name", compiler::cpp::FieldName(field));

  std::string s;
  if (type == FieldDescriptor::Type::TYPE_STRING || type == FieldDescriptor::Type::TYPE_BYTES) {
    generation::helpers::PushStringType(field, &formatter);
    formatter("$container_type$ $field_name$;\n");
  } else {
    formatter("$1$ $field_name$;\n", field_type);
  }
}

void WriteRepeatedField(const FieldDescriptor *const field, io::Printer *const printer) {
  compiler::cpp::Formatter formatter{printer};
  generation::helpers::PushRepeatedType(field, &formatter);
  formatter("$container_type$ $1$;\n", compiler::cpp::FieldName(field));
}

void WriteMapField(const FieldDescriptor *const field, io::Printer *const printer) {
  compiler::cpp::Formatter formatter{printer};
  generation::helpers::PushMapType(field, &formatter);
  formatter("$container_type$ $1$;\n", compiler::cpp::FieldName(field));
}

// Generation for primitive types
void GenerateForPrimitive(const FieldDescriptor *const field, io::Printer *const printer) {
  compiler::cpp::Formatter formatter{printer};
  formatter.Set("field_name", compiler::cpp::UnderscoresToCamelCase(field->name(), true));
  formatter.Set("field_type", compiler::cpp::PrimitiveTypeName(field->cpp_type()));

  const auto type{field->type()};
  if (type == FieldDescriptor::Type::TYPE_STRING || type == FieldDescriptor::Type::TYPE_BYTES) {
    generation::helpers::PushStringType(field, &formatter);
    formatter(
        "constexpr void Set$field_name$(const $container_type$ &value);\n"
        "constexpr void Set$field_name$($container_type$ &&value) noexcept;\n"
        "[[nodiscard]] constexpr const $container_type$ &$field_name$() const noexcept;\n"
        "constexpr void Clear$field_name$() noexcept;\n"
        "[[nodiscard]] constexpr bool Has$field_name$() const noexcept;\n");
  } else {
    formatter(
        "constexpr void Set$field_name$($field_type$ value) noexcept;\n"
        "[[nodiscard]] constexpr $field_type$ $field_name$() const noexcept;\n"
        "constexpr void Clear$field_name$() noexcept;\n"
        "[[nodiscard]] constexpr bool Has$field_name$() const noexcept;\n");
  }
}

// Generation for repeated/map
void GenerateForContainer(const FieldDescriptor *const field, compiler::cpp::Formatter *const format) {
  (*format)(
      "[[nodiscard]] constexpr $container_type$ &$1$() noexcept;\n"
      "[[nodiscard]] constexpr const $container_type$ &$1$() const noexcept;\n",
      compiler::cpp::UnderscoresToCamelCase(field->name(), true));
}

// Generation for messages
void GenerateForMessage(const FieldDescriptor *const field, io::Printer *const printer) {
  const auto type{field->message_type()};
  printer->Print(
      "[[nodiscard]] constexpr bool Has$field_type$() const noexcept;\n"
      "[[nodiscard]] constexpr $field_type$ &Get$field_type$() noexcept;\n"
      "[[nodiscard]] constexpr const $field_type$ &Get$field_type$() const noexcept;\n",
      "field_type", type->name());
}

void GenerateMethodsForField(const FieldDescriptor *const field, io::Printer *const printer) {
  const auto field_name{field->name()};
  printer->Print("\n// methods for '$field_name$' field\npublic:\n", "field_name", compiler::cpp::FieldName(field));
  printer->Indent();

  if (field->is_map()) {
    compiler::cpp::Formatter formatter{printer};
    generation::helpers::PushMapType(field, &formatter);
    GenerateForContainer(field, &formatter);
  } else if (field->is_repeated()) {
    compiler::cpp::Formatter formatter{printer};
    generation::helpers::PushRepeatedType(field, &formatter);
    GenerateForContainer(field, &formatter);
  } else if (field->type() == FieldDescriptor::Type::TYPE_MESSAGE) {
    GenerateForMessage(field, printer);
  } else {
    GenerateForPrimitive(field, printer);
  }

  printer->Outdent();
}

void GenerateMethodsForFields(const Descriptor *const type, io::Printer *const printer) {
  const auto kFieldCount{type->field_count()};
  for (int field_index{}; field_index < kFieldCount; ++field_index) {
    GenerateMethodsForField(type->field(field_index), printer);
  }
}

[[nodiscard]] bool GenerateFields(const Descriptor *const type, io::Printer *const printer, std::string *const error) {
  printer->Print("\n// fields\nprivate:\n");
  printer->Indent();

  const auto kFieldsCount{type->field_count()};
  std::size_t optional_count{};
  for (int field_index{}; field_index < kFieldsCount; ++field_index) {
    const auto field{type->field(field_index)};
    auto deb{field->options().ByteSizeLong()};
    if (field->is_optional()) {
      ++optional_count;
      WriteField(field, printer);
    } else if (field->is_map()) {  // order does matter: maps are repeated
      WriteMapField(field, printer);
    } else if (field->is_repeated()) {
      WriteRepeatedField(field, printer);
    } else {
      error->assign("unsupported field qualifier");
      return false;
    }
  }

  if (optional_count > 0) {
    printer->Print("\nstd::byte optional_fields[$count$]{};\n", "count",
                   std::to_string(optional_count / 8 + ((optional_count % 8) != 0)));
  }

  printer->Outdent();
}

void GenerateDefaultMethods(const Descriptor *const type, io::Printer *const printer) {
  printer->Print("// constructors, copy/move operators/constructors, spaceship operator\npublic:\n");
  printer->Indent();
  printer->Print(
      "// constructor\n"
      "constexpr $class_name$() = default;\n\n"
      "// desctructor\n"
      "constexpr ~$class_name$() = default;\n\n"
      "// comparassion operators\n"
      "constexpr auto operator<=>(const $class_name$ &) const = default;\n\n"
      "// copy/move constructors\n"
      "constexpr $class_name$(const $class_name$ &) = default;\n"
      "constexpr $class_name$($class_name$ &&) = default;\n\n"
      "// copy/move operators\n"
      "constexpr $class_name$ &operator=(const $class_name$ &) = default;\n"
      "constexpr $class_name$ &operator=($class_name$ &&) = default;\n",
      "class_name", compiler::cpp::ClassName(type));
  printer->Outdent();
}

void GenerateJsonMethods(io::Printer *const printer) {
  printer->Indent();
  printer->Print(
      "// json convertions vvvvvv\n"
      "[[nodiscard]] inline nlohmann::json ToJson() const;\n"
      "inline void FromJson(const nlohmann::json &value);\n"
      "// json convertions ^^^^^^\n");
  printer->Outdent();
}

[[nodiscard]] bool AppendMessage(const Descriptor *const type, io::Printer *const printer, std::string *error) {
  if (!CheckConstraints(type, error)) {
    return false;
  }

  printer->Print("class $class_name$ {\n", "class_name", compiler::cpp::ClassName(type));
  printer->Indent();

  GenerateDefaultMethods(type, printer);
  GenerateMethodsForFields(type, printer);
  GenerateJsonMethods(printer);

  if (!GenerateFields(type, printer, error)) {
    return false;
  }

  printer->Outdent();
  printer->Print("};\n");  // close class
  return true;
}

void AddStandardIncludes(io::Printer *const printer) {
  printer->Print(
      "#include <cstddef>\n"
      "#include <utility>\n"
      "#include \"nlohmann/json.hpp\"\n");
}

[[nodiscard]] bool AddStringIncludes(const FileDescriptor *const file, io::Printer *const printer) {
  if (generation::helpers::HasString(file)) {
    printer->Print("#include <string>\n");
    return true;
  }
  return false;
}

[[nodiscard]] bool AddRepeatedFieldsIncludes(const FileDescriptor *const file, io::Printer *const printer) {
  if (compiler::cpp::HasRepeatedFields(file)) {
    printer->PrintRaw("#include <vector>  // for repeated fields\n");
    return true;
  }
  return false;
}

[[nodiscard]] bool AddMapFieldIncludes(const FileDescriptor *const file, io::Printer *const printer) {
  if (compiler::cpp::HasMapFields(file)) {
    printer->PrintRaw("#include <map>  // for map fields\n");
    return true;
  }
  return false;
}

void AddAllocatorIncludes(io::Printer *const printer) {
  printer->PrintRaw("#include <utility/memory/allocator.hpp>  // from common library\n");
}

void AddDependencies(const google::protobuf::FileDescriptor *const file, compiler::cpp::Formatter *const formatter) {
  const auto count{file->dependency_count()};
  for (std::remove_const_t<decltype(count)> index{}; index < count; ++index) {
    (*formatter)("#include \"$1$\"\n", compiler::StripProto(file->dependency(index)->name()) + ".pb.h");
  }
}
}  // namespace

namespace generation {
bool HeaderGenerator::GenerateHeader(const google::protobuf::FileDescriptor *const file,
                                     io::ZeroCopyOutputStream *const output, std::string *const error) const {
  io::Printer printer{output, '$'};
  printer.Print("// This file is generated by protobuf compiler. DO NOT MODIFY IT!!\n");
  printer.Print("#pragma once\n");

  compiler::cpp::Formatter format{&printer};

  // print additional includes
  AddStandardIncludes(&printer);
  bool need_allocator{AddRepeatedFieldsIncludes(file, &printer)};
  need_allocator |= AddMapFieldIncludes(file, &printer);
  need_allocator |= AddStringIncludes(file, &printer);
  if (need_allocator) {
    printer.Print("\n");
    AddAllocatorIncludes(&printer);
  }
  printer.Print("\n");
  AddDependencies(file, &format);
  printer.Print("\n");

  // print file namespace
  compiler::cpp::NamespaceOpener nm{compiler::cpp::Namespace(file, {}), format};

  // print all messages
  const auto kMessagesTypesCount{file->message_type_count()};
  for (int type_index{}; type_index < kMessagesTypesCount; ++type_index) {
    if (!AppendMessage(file->message_type(type_index), &printer, error)) {
      return false;
    }
  }
  return true;
}
}  // namespace generation
